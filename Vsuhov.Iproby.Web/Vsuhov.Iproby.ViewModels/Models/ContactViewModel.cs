﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Vsuhov.Iproby.ViewModels
{
    public class ContactViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Пожалуйста введите имя")]
        public string FirstName { get; set; }
        public string LastName { get; set;}
        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Пожалуйста введите адрес")]
        public string Address { get; set; }
        public string Skype { get; set; }
        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Пожалуйста введите телефон")]
        public string Mobile { get; set; }
        public string Facebook { get; set; }
        public string Vkontakte { get; set; }
        public string Icq { get; set; }
        public string Site { get; set; }
        public string Avatar { get; set; }

    }
}
