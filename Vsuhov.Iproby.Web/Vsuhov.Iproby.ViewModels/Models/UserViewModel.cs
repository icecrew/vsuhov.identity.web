﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vsuhov.Iproby.ViewModels
{
    public class UserViewModel: IdentityUser
    {
        public int Id { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Пожалуйста введите email")]
        public string Email { get; set; }
        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Пожалуйста введите пароль")]
        public string Password { get; set; }
        public DateTime DateFrom { get; set; }
        [Required]
        public ContactViewModel contact { get; set; }
    }
}
