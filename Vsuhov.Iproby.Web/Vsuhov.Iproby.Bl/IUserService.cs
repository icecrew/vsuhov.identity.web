﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Iproby.ViewModels;

namespace Vsuhov.Iproby.Bl
{
    public interface IUserService
    {
        UserViewModel GetUser(int id);

        int AddUser(UserViewModel model);

        int UpdateUser(UserViewModel model);

    }
}
