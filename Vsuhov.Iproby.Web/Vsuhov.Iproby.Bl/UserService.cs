﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Iproby.Dal;
using Vsuhov.Iproby.ViewModels;

namespace Vsuhov.Iproby.Bl
{
    public class UserService : IUserService
    {
        public int AddUser(UserViewModel model)
        {
            var context = new UserContext();
            return context.AddUser(model);
        }

        public UserViewModel GetUser(int id)
        {
            var context = new UserContext();
            return context.GetUser(id);
        }

        public int UpdateUser(UserViewModel model)
        {
            var context = new UserContext();
            return context.UpdateUser(model);
        }
    }
}
