﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Iproby.ViewModels;

namespace Vsuhov.Iproby.Dal
{
    public interface IUserContext
    {
        List<UserViewModel> GetUsers();
        UserViewModel GetUser(int id);
        int AddUser(UserViewModel model);
        int UpdateUser(UserViewModel model);
    }
}
