//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vsuhov.Iproby.Dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class payments
    {
        public int id { get; set; }
        public int announ_id { get; set; }
        public int customer_id { get; set; }
        public string mrchlogin { get; set; }
        public string outsum { get; set; }
        public Nullable<int> invid { get; set; }
        public string description { get; set; }
        public string signaturevalue { get; set; }
        public Nullable<System.DateTime> date_from { get; set; }
        public string status { get; set; }
        public Nullable<System.DateTime> RequestDate { get; set; }
        public Nullable<System.DateTime> StateDate { get; set; }
        public string Info { get; set; }
        public string IncCurrLabel { get; set; }
        public Nullable<double> IncSum { get; set; }
        public Nullable<int> IncAccount { get; set; }
        public string PaymentMethod { get; set; }
        public string Code { get; set; }
        public string OutCurrLabel { get; set; }
        public string state { get; set; }
    
        public virtual announ announ { get; set; }
        public virtual customers customers { get; set; }
    }
}
