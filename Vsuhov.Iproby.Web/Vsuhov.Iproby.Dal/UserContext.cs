﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Iproby.ViewModels;

namespace Vsuhov.Iproby.Dal
{
    public class UserContext : IUserContext
    {
        public int AddUser(UserViewModel model)
        {
            using (var db = new iproby94_cust_dbEntities())
            {
                if (model != null) {
                    contact contact = new contact();
                    contact.address = model.contact.Address;
                    contact.avatar = model.contact.Avatar;
                    contact.email = model.Email;
                    contact.facebook = model.contact.Facebook;
                    contact.first_name = model.contact.FirstName;
                    contact.icq = model.contact.Icq;
                    contact.last_name = model.contact.LastName;
                    contact.mobile = model.contact.Mobile;
                    contact.site = model.contact.Site;
                    contact.skype = model.contact.Skype;
                    contact.vkontakte = model.contact.Site;
                    db.contact.Add(contact);
                    db.SaveChanges();
                    int contact_id = contact.contact_id;
                    customers customer = new customers();
                    customer.login = model.Email;
                    customer.password = model.Password;
                    customer.date_from = DateTime.Now;
                    customer.contact_id = contact_id;
                    db.customers.Add(customer);
                    db.SaveChanges();
                    return 1;
                }else { return -1; }
            }
            
        }

        public UserViewModel GetUser(int id)
        {
            var result = new UserViewModel();
            using (var db=new iproby94_cust_dbEntities())
            {
                var items = from user in db.customers
                            where user.customer_id == id
                            join cont in db.contact on user.contact_id equals cont.contact_id into contGroup
                            select new { usr=user,cnt=contGroup };
                foreach(var item in items)
                {
                    var contact = new ContactViewModel();
                    foreach (var cont in item.cnt)
                    {
                        contact.Address = cont.address;
                        contact.Avatar = cont.avatar;
                        contact.Facebook = cont.facebook;
                        contact.FirstName = cont.first_name;
                        contact.Icq = cont.icq;
                        contact.LastName = cont.last_name;
                        contact.Mobile = cont.mobile;
                        contact.Site = cont.site;
                        contact.Skype = cont.skype;
                        contact.Vkontakte = cont.vkontakte;
                        
                    }

                    result.contact = contact;
                    result.DateFrom = item.usr.date_from;
                    result.Email = item.usr.login;
                    result.Password = item.usr.password;
                    
                }
            }
            if (result != null)
                return result;
            else return new UserViewModel();
        }

        public List<UserViewModel> GetUsers()
        {
            throw new NotImplementedException();
        }

        public int UpdateUser(UserViewModel model)
        {
            using (var db = new iproby94_cust_dbEntities())
            {
                var user = new customers();
                var items = from linq in db.customers
                           where linq.customer_id == model.Id
                           select linq;
                foreach (var item in items)
                {
                    user = item;
                }
                user.login = model.Email;
                user.password = model.Password;
                user.date_from = DateTime.Now;
                db.SaveChanges();
                var contact = new contact();
                var itemsCont = from linq in db.contact
                                where linq.contact_id == model.contact.Id
                                select linq;
                foreach (var itemCont in itemsCont)
                {
                    contact = itemCont;
                }
                contact.address = model.contact.Address;
                contact.avatar = model.contact.Avatar;
                contact.facebook = model.contact.Facebook;
                contact.first_name = model.contact.FirstName;
                contact.icq = model.contact.Icq;
                contact.last_name = model.contact.LastName;
                contact.mobile = model.contact.Mobile;
                contact.site = model.contact.Site;
                contact.skype = model.contact.Skype;
                contact.vkontakte = model.contact.Vkontakte;
                db.SaveChanges();

            }
            return 1;
        }
    }
}
