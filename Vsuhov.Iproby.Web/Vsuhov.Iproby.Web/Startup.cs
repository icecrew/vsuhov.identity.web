﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Vsuhov.Iproby.Web.Startup))]
namespace Vsuhov.Iproby.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
